#ifndef MANAGERDICTIONARIES_H
#define MANAGERDICTIONARIES_H

#include <QVector>
#include <QMap>

struct DICTIONARY_IMAGES {
    QString image;
    QMap<QString,QString> answer1;
    QMap<QString,QString> answer2;
    QMap<QString,QString> answer3;
    QMap<QString,QString> answer4;
};

struct DICTIONARY {
    QString filename;
    QString folder;
    QString author;
    QMap<QString,QString> description;

    QVector<DICTIONARY_IMAGES> listImages;
};

class ManagerDictionaries {
    public:
        static DICTIONARY loadDictionary(QString filename);
};

#endif // MANAGERDICTIONARIES_H
