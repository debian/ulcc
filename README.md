# ENGLISH #

ulcc is copyright by DanSoft (dik@inbox.ru), 2018-2019

This program is licensed under the terms and conditions of
the GNU General Public License (GPLv3+); either version
3 of the License, or (at your option) any later version.
Please read the 'COPYING' file for more information.

ulcc is an educational tool/game targeted at children.

To build ulcc you need Qt5 and qmake.
Full list of dependencies for linux: see pkg/ulcc.spec.

### Building: ###
~~~~
qmake
make
~~~~
Also you can use Qt Creator for building.
Just open ulcc.pro and build ulcc (`Ctrl+B`).

### Installation (under root): ###
~~~~
make install
~~~~

### Testing without installation: ###
~~~~
qmake QMAKE_CXXFLAGS+='-DGLOBAL_PATH_USERDATA=\\\"..\\\"'
make
./Bin/ulcc
~~~~

Applications for Windows and sources:
https://bitbucket.org/admsasha/ulcc/downloads

Translations:
https://www.transifex.com/Magic/ulcc

![screenshot1 images](https://bitbucket.org/admsasha/ulcc/raw/master/screenshots/screenshot1.png)

***

# RUSSIAN/РУССКИЙ #
Авторское право на ulcc принадлежит DanSoft (dik@inbox.ru),
2018-2019

Эта программа лицензирована в соответствии с положениями GNU
General Public License (GPLv3+); либо версии 3 Лицензии, либо
(по вашему выбору) любой более поздней версии. Пожалуйста,
прочитайте файл 'COPYING' для получения дополнительной
информации.

ulcc - это образовательная утилита/игра, ориентированная на
детей. 

Для сборки ulcc вам нужны Qt5 и qmake.
Полный список зависимостей для linux: см. pkg/ulcc.spec.

### Сборка: ###
~~~~
qmake
make
~~~~
Также вы можете использовать Qt Creator для сборки.
Просто откройте ulcc.pro и соберите ulcc (`Ctrl+B`).

### Установка (под root): ###
~~~~
make install
~~~~

### Тестирование без установки: ###
~~~~
qmake QMAKE_CXXFLAGS+='-DGLOBAL_PATH_USERDATA=\\\"..\\\"'
make
./Bin/ulcc
~~~~

Приложения для Windows и сурсы:
https://bitbucket.org/admsasha/ulcc/downloads

Переводы:
https://www.transifex.com/Magic/ulcc

![screenshot2 images](https://bitbucket.org/admsasha/ulcc/raw/master/screenshots/screenshot2.png)
