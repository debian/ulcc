#ifndef MANAGERLANGUAGE_H
#define MANAGERLANGUAGE_H

#include <QObject>
#include <QMap>
#include <QJsonObject>

class ManagerLanguage : public QObject {
    Q_OBJECT
    public:
        ManagerLanguage();

        QString getNativeName(QString lang_code);


    private:
        QJsonObject rootLangs;
};

#endif // MANAGERLANGUAGE_H
