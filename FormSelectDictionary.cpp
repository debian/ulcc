#include "FormSelectDictionary.h"
#include "ui_FormSelectDictionary.h"

#include <QDir>
#include <QFile>
#include <QRegExp>
#include <QDebug>
#include <QSettings>

#include "config_ulcc.h"
#include "ManagerDictionaries.h"
#include "ManagerLanguage.h"


FormSelectDictionary::FormSelectDictionary(QWidget *parent) : QDialog(parent), ui(new Ui::FormSelectDictionary){
    ui->setupUi(this);

    this->setFixedSize(this->width(),this->height());
    this->setWindowTitle(tr("Select dictionary"));
    this->setWindowIcon(QIcon(QString(GLOBAL_PATH_USERDATA)+"/images/icons/ulcc.png"));

    // open ini user config
    QDir dirConfig(QDir::homePath()+"/.ulcc/");
    if (dirConfig.exists()==false) dirConfig.mkpath(QDir::homePath()+"/.ulcc/");
    QSettings confSettings(dirConfig.absoluteFilePath("settings.ini"), QSettings::IniFormat);
    confSettings.setPath(QSettings::IniFormat, QSettings::UserScope, QDir::currentPath());

    QString selectDictionary = confSettings.value("Dictionary/Dictionary","").toString();

    QDir dir(QString(GLOBAL_PATH_USERDATA)+"/dictionaries/");
    dir.setFilter(QDir::Dirs | QDir::Hidden | QDir::NoDotAndDotDot);
    QFileInfoList list = dir.entryInfoList();
    for (QFileInfo fileInfo : list) {
        DICTIONARY dictionary = ManagerDictionaries::loadDictionary(fileInfo.absoluteFilePath()+"/dictionary.json");

        // default
        QString dictionaryDescription = dictionary.description[""];

        // find for en locale
        if (dictionary.description.contains("en_EN")) dictionaryDescription = dictionary.description["en_EN"];

        // find for current locale
        if (dictionary.description.contains(QLocale::system().name())) dictionaryDescription = dictionary.description[QLocale::system().name()];

        ui->comboBox->addItem(dictionaryDescription,fileInfo.fileName());

        if (selectDictionary==fileInfo.fileName()){
            ui->comboBox->setCurrentIndex(ui->comboBox->count()-1);
        }

    }

    refreshLanguage();

    connect(ui->comboBox,SIGNAL(currentIndexChanged(int)),this,SLOT(comboBoxCurrentIndexChanged(int)));
    connect(ui->pushButton,SIGNAL(clicked(bool)),this,SLOT(reject()));
    connect(ui->pushButton_2,SIGNAL(clicked(bool)),this,SLOT(accept()));
}

FormSelectDictionary::~FormSelectDictionary(){
    delete ui;
}

QString FormSelectDictionary::getDictionary(){
    return ui->comboBox->currentData().toString();
}

QString FormSelectDictionary::getLanguage(){
    return ui->comboBox_2->currentData().toString();
}

void FormSelectDictionary::refreshLanguage(){   
    QString selectLanguageDictionary = ui->comboBox_2->currentData().toString();

    ui->comboBox_2->clear();

    ManagerLanguage mngLanguage;

    ui->comboBox_2->addItem(tr("System language"),"system");

    QDir dir(QString(GLOBAL_PATH_USERDATA)+"/dictionaries/"+ui->comboBox->currentData().toString()+"/languages/");
    dir.setFilter(QDir::Files | QDir::Hidden | QDir::NoDotAndDotDot);
    QRegExp rx("^(.*)(.lng)$");
    QFileInfoList list = dir.entryInfoList();
    for (QFileInfo fileInfo : list) {
        if (rx.indexIn(fileInfo.fileName())!=-1){
            QString string_lang = mngLanguage.getNativeName(rx.cap(1));
            ui->comboBox_2->addItem(string_lang,rx.cap(1));
            if (selectLanguageDictionary==rx.cap(1)){
                ui->comboBox_2->setCurrentIndex(ui->comboBox_2->count()-1);
            }
        }
    }
}

void FormSelectDictionary::comboBoxCurrentIndexChanged(int){
    refreshLanguage();
}

