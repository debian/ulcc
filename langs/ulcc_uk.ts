<?xml version="1.0" ?><!DOCTYPE TS><TS language="uk" version="2.1">
<context>
    <name>FormAbout</name>
    <message>
        <location filename="../FormAbout.ui" line="31"/>
        <source>Teaching children by pictures</source>
        <translation>Навчання дітей за малюнками</translation>
    </message>
    <message>
        <location filename="../FormAbout.ui" line="131"/>
        <source>License: GPLv3+</source>
        <translation>Умови ліцензування: GPLv3+</translation>
    </message>
    <message>
        <location filename="../FormAbout.ui" line="208"/>
        <source>Visit web site</source>
        <translation>Відвідати сайт</translation>
    </message>
    <message>
        <location filename="../FormAbout.ui" line="226"/>
        <source>Close</source>
        <translation>Закрити</translation>
    </message>
    <message>
        <location filename="../FormAbout.cpp" line="15"/>
        <source>About</source>
        <translation>Про проґраму</translation>
    </message>
    <message>
        <location filename="../FormAbout.cpp" line="18"/>
        <source>Version:</source>
        <translation>Версія:</translation>
    </message>
    <message>
        <location filename="../FormAbout.cpp" line="19"/>
        <source>Date build:</source>
        <translation>Дата збирання:</translation>
    </message>
    <message>
        <location filename="../FormAbout.cpp" line="20"/>
        <source>All rights reserved.</source>
        <translation>Усі права захищено.</translation>
    </message>
</context>
<context>
    <name>FormHelp</name>
    <message>
        <location filename="../FormHelp.ui" line="48"/>
        <source>OK</source>
        <translation>Гаразд</translation>
    </message>
    <message>
        <location filename="../FormHelp.cpp" line="12"/>
        <source>Help</source>
        <translation>Довідка</translation>
    </message>
    <message>
        <location filename="../FormHelp.cpp" line="16"/>
        <source>How to use:</source>
        <translation>Як користуватися:</translation>
    </message>
    <message>
        <location filename="../FormHelp.cpp" line="17"/>
        <source>Choose the correct answer from the answers provided.
</source>
        <translation>Виберіть правильну відповідь серед варіантів відповідей.
</translation>
    </message>
</context>
<context>
    <name>FormSelectDictionary</name>
    <message>
        <location filename="../FormSelectDictionary.ui" line="26"/>
        <source>Dictionary</source>
        <translation>Словник</translation>
    </message>
    <message>
        <location filename="../FormSelectDictionary.ui" line="59"/>
        <source>Languages</source>
        <translation>Мови</translation>
    </message>
    <message>
        <location filename="../FormSelectDictionary.ui" line="72"/>
        <source>Cancel</source>
        <translation>Скасувати</translation>
    </message>
    <message>
        <location filename="../FormSelectDictionary.ui" line="85"/>
        <source>Select</source>
        <translation>Позначити</translation>
    </message>
    <message>
        <location filename="../FormSelectDictionary.cpp" line="19"/>
        <source>Select dictionary</source>
        <translation>Вибір словника</translation>
    </message>
    <message>
        <location filename="../FormSelectDictionary.cpp" line="69"/>
        <source>System language</source>
        <translation>Мова системи</translation>
    </message>
</context>
<context>
    <name>FormSelectLanguage</name>
    <message>
        <location filename="../FormSelectLanguage.ui" line="14"/>
        <source>Dialog</source>
        <translation>Діялоґ</translation>
    </message>
    <message>
        <location filename="../FormSelectLanguage.ui" line="26"/>
        <source>Cancel</source>
        <translation>Відміна</translation>
    </message>
    <message>
        <location filename="../FormSelectLanguage.ui" line="39"/>
        <source>OK</source>
        <translation>Гаразд</translation>
    </message>
    <message>
        <location filename="../FormSelectLanguage.ui" line="57"/>
        <source>Select language:</source>
        <translation>Вибрати мову:</translation>
    </message>
    <message>
        <location filename="../FormSelectLanguage.cpp" line="20"/>
        <source>Select language</source>
        <translation>Вибрати мову</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="21"/>
        <location filename="../mainwindow.cpp" line="359"/>
        <source>Teaching children by pictures</source>
        <translation>Навчання дітей за малюнками</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="31"/>
        <location filename="../mainwindow.cpp" line="362"/>
        <source>Please select the dictionary</source>
        <translation>Будь ласка, виберіть словник</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="39"/>
        <location filename="../mainwindow.cpp" line="267"/>
        <location filename="../mainwindow.cpp" line="368"/>
        <source>Info</source>
        <translation>Інфо</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="40"/>
        <location filename="../mainwindow.cpp" line="268"/>
        <location filename="../mainwindow.cpp" line="323"/>
        <location filename="../mainwindow.cpp" line="324"/>
        <location filename="../mainwindow.cpp" line="369"/>
        <source>Help</source>
        <translation>Довідка</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="329"/>
        <location filename="../mainwindow.cpp" line="330"/>
        <source>Exit</source>
        <translation>Вийти</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="147"/>
        <location filename="../mainwindow.cpp" line="372"/>
        <source>Next</source>
        <translation>Далі</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="37"/>
        <location filename="../mainwindow.cpp" line="265"/>
        <location filename="../mainwindow.cpp" line="366"/>
        <source>Select dictionary</source>
        <translation>Вибір словника</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="38"/>
        <location filename="../mainwindow.cpp" line="266"/>
        <location filename="../mainwindow.cpp" line="367"/>
        <source>Select language</source>
        <translation>Вибрати мову</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="151"/>
        <source>Total:</source>
        <translation>Загалом:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="152"/>
        <source>Correct:</source>
        <translation>Правильно:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="153"/>
        <source>Wrong:</source>
        <translation>Неправильно:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="317"/>
        <location filename="../mainwindow.cpp" line="318"/>
        <source>Dictionary</source>
        <translation>Словник</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="320"/>
        <source>Language</source>
        <translation>Мова</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="321"/>
        <source>Languages</source>
        <translation>Мови</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="326"/>
        <location filename="../mainwindow.cpp" line="327"/>
        <source>About</source>
        <translation>Про проґраму</translation>
    </message>
</context>
<context>
    <name>ManagerLanguage</name>
    <message>
        <location filename="../ManagerLanguage.cpp" line="16"/>
        <source>Error while opening</source>
        <translation>Помилка під час спроби відкрити</translation>
    </message>
    <message>
        <location filename="../ManagerLanguage.cpp" line="23"/>
        <source>is not valid</source>
        <translation>не є коректним</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../ManagerDictionaries.cpp" line="22"/>
        <source>Error while opening</source>
        <translation>Помилка під час спроби відкрити</translation>
    </message>
    <message>
        <location filename="../ManagerDictionaries.cpp" line="30"/>
        <source>is not valid</source>
        <translation>не є коректним</translation>
    </message>
</context>
</TS>