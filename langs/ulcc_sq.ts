<?xml version="1.0" ?><!DOCTYPE TS><TS language="sq" version="2.1">
<context>
    <name>FormAbout</name>
    <message>
        <location filename="../FormAbout.ui" line="31"/>
        <source>Teaching children by pictures</source>
        <translation>Mësoni fëmijët me fotografi</translation>
    </message>
    <message>
        <location filename="../FormAbout.ui" line="131"/>
        <source>License: GPLv3+</source>
        <translation>Liçensë: GPLv3+</translation>
    </message>
    <message>
        <location filename="../FormAbout.ui" line="208"/>
        <source>Visit web site</source>
        <translation>Vizito faqen</translation>
    </message>
    <message>
        <location filename="../FormAbout.ui" line="226"/>
        <source>Close</source>
        <translation>Mbyll</translation>
    </message>
    <message>
        <location filename="../FormAbout.cpp" line="15"/>
        <source>About</source>
        <translation>Rreth</translation>
    </message>
    <message>
        <location filename="../FormAbout.cpp" line="18"/>
        <source>Version:</source>
        <translation>Versioni:</translation>
    </message>
    <message>
        <location filename="../FormAbout.cpp" line="19"/>
        <source>Date build:</source>
        <translation>Data ndërtuar:</translation>
    </message>
    <message>
        <location filename="../FormAbout.cpp" line="20"/>
        <source>All rights reserved.</source>
        <translation>Të gjitha të drejtat e rezervuara.</translation>
    </message>
</context>
<context>
    <name>FormHelp</name>
    <message>
        <location filename="../FormHelp.ui" line="48"/>
        <source>OK</source>
        <translation>Dakord</translation>
    </message>
    <message>
        <location filename="../FormHelp.cpp" line="12"/>
        <source>Help</source>
        <translation>Ndihmë</translation>
    </message>
    <message>
        <location filename="../FormHelp.cpp" line="16"/>
        <source>How to use:</source>
        <translation>Si ti përdoret:</translation>
    </message>
    <message>
        <location filename="../FormHelp.cpp" line="17"/>
        <source>Choose the correct answer from the answers provided.
</source>
        <translation>Zgjidhni përgjigjen e saktë nga përgjigjet e dhëna.
</translation>
    </message>
</context>
<context>
    <name>FormSelectDictionary</name>
    <message>
        <location filename="../FormSelectDictionary.ui" line="26"/>
        <source>Dictionary</source>
        <translation>Fjalor</translation>
    </message>
    <message>
        <location filename="../FormSelectDictionary.ui" line="59"/>
        <source>Languages</source>
        <translation>Gjuhë</translation>
    </message>
    <message>
        <location filename="../FormSelectDictionary.ui" line="72"/>
        <source>Cancel</source>
        <translation>Anulo</translation>
    </message>
    <message>
        <location filename="../FormSelectDictionary.ui" line="85"/>
        <source>Select</source>
        <translation>Përzgjedh</translation>
    </message>
    <message>
        <location filename="../FormSelectDictionary.cpp" line="19"/>
        <source>Select dictionary</source>
        <translation>Përzgjedh fjalor</translation>
    </message>
    <message>
        <location filename="../FormSelectDictionary.cpp" line="69"/>
        <source>System language</source>
        <translation>Gjuha sistemit</translation>
    </message>
</context>
<context>
    <name>FormSelectLanguage</name>
    <message>
        <location filename="../FormSelectLanguage.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../FormSelectLanguage.ui" line="26"/>
        <source>Cancel</source>
        <translation>Anulo</translation>
    </message>
    <message>
        <location filename="../FormSelectLanguage.ui" line="39"/>
        <source>OK</source>
        <translation>Dakord</translation>
    </message>
    <message>
        <location filename="../FormSelectLanguage.ui" line="57"/>
        <source>Select language:</source>
        <translation>Përzgjedh gjuhë:</translation>
    </message>
    <message>
        <location filename="../FormSelectLanguage.cpp" line="20"/>
        <source>Select language</source>
        <translation>Përzgjedh gjuhë</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="21"/>
        <location filename="../mainwindow.cpp" line="359"/>
        <source>Teaching children by pictures</source>
        <translation>Mësoni fëmijët me fotografi</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="31"/>
        <location filename="../mainwindow.cpp" line="362"/>
        <source>Please select the dictionary</source>
        <translation>Ju lutem përzgjidhni një fjalor</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="39"/>
        <location filename="../mainwindow.cpp" line="267"/>
        <location filename="../mainwindow.cpp" line="368"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="40"/>
        <location filename="../mainwindow.cpp" line="268"/>
        <location filename="../mainwindow.cpp" line="323"/>
        <location filename="../mainwindow.cpp" line="324"/>
        <location filename="../mainwindow.cpp" line="369"/>
        <source>Help</source>
        <translation>Ndihmë</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="329"/>
        <location filename="../mainwindow.cpp" line="330"/>
        <source>Exit</source>
        <translation>Dalje</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="147"/>
        <location filename="../mainwindow.cpp" line="372"/>
        <source>Next</source>
        <translation>Tjetër</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="37"/>
        <location filename="../mainwindow.cpp" line="265"/>
        <location filename="../mainwindow.cpp" line="366"/>
        <source>Select dictionary</source>
        <translation>Përzgjedh fjalor</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="38"/>
        <location filename="../mainwindow.cpp" line="266"/>
        <location filename="../mainwindow.cpp" line="367"/>
        <source>Select language</source>
        <translation>Përzgjedh gjuhë</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="151"/>
        <source>Total:</source>
        <translation>Total:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="152"/>
        <source>Correct:</source>
        <translation>Saktë:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="153"/>
        <source>Wrong:</source>
        <translation>Gabim:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="317"/>
        <location filename="../mainwindow.cpp" line="318"/>
        <source>Dictionary</source>
        <translation>Fjalor</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="320"/>
        <source>Language</source>
        <translation>Gjuhë</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="321"/>
        <source>Languages</source>
        <translation>Gjuhë</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="326"/>
        <location filename="../mainwindow.cpp" line="327"/>
        <source>About</source>
        <translation>Rreth</translation>
    </message>
</context>
<context>
    <name>ManagerLanguage</name>
    <message>
        <location filename="../ManagerLanguage.cpp" line="16"/>
        <source>Error while opening</source>
        <translation>Gabim gjatë hapjes</translation>
    </message>
    <message>
        <location filename="../ManagerLanguage.cpp" line="23"/>
        <source>is not valid</source>
        <translation>nuk është e vlefshme</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../ManagerDictionaries.cpp" line="22"/>
        <source>Error while opening</source>
        <translation>Gabim gjatë hapjes</translation>
    </message>
    <message>
        <location filename="../ManagerDictionaries.cpp" line="30"/>
        <source>is not valid</source>
        <translation>nuk është e vlefshme</translation>
    </message>
</context>
</TS>