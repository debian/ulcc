#ifndef FORMSELECTDICTIONARY_H
#define FORMSELECTDICTIONARY_H

#include <QDialog>

namespace Ui {
class FormSelectDictionary;
}

class FormSelectDictionary : public QDialog {
    Q_OBJECT

    public:
        explicit FormSelectDictionary(QWidget *parent = nullptr);
        ~FormSelectDictionary();

        QString getDictionary();
        QString getLanguage();

    private:
        Ui::FormSelectDictionary *ui;

        void refreshLanguage();

    private slots:
        void comboBoxCurrentIndexChanged(int);
};

#endif // FORMSELECTDICTIONARY_H
