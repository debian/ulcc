#ifndef CONFIG_H
#define CONFIG_H

#include <QString>

#define ULCC_VERSION "1.0.2"
#define ULCC_DATEBUILD "03.09.2020"

#if !defined(GLOBAL_PATH_USERDATA)
#if defined(__WIN32__)
#define GLOBAL_PATH_USERDATA "."
#else
#define GLOBAL_PATH_USERDATA "/usr/share/ulcc"
#endif
#endif

#endif // CONFIG_H
