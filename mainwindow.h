#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QToolBar>
#include <QSettings>
#include <QPushButton>
#include <QTranslator>

#include "ManagerDictionaries.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow{
    Q_OBJECT

    public:
        explicit MainWindow(QWidget *parent = nullptr);
        ~MainWindow();

        void setColorPushButton(QPushButton *button, QString color);

    private:
        Ui::MainWindow *ui;

        void initToolBar();
        void refreshTranslate();
        void refreshDynamicPushButtonText();

        void modifyPosition();

        bool loadDictionary(QString dictionary);
        void refreshImage();
        void refreshScore();

        void startGame();

        QSettings *confSettings;

        DICTIONARY currentDictionary;

        QString selectDictionary;
        QString selectLanguageDictionary;

        int currentIndexDictionaryImage;
        int indexCorrectAnswer;

        int m_correct;
        int m_total;
        int m_wrong;

        bool waitAnswer;
        bool runGame;

        // for translate
        QTranslator translator;
        QTranslator qtTranslator;
        QMap<QWidget*,QMap<QString,QString>> listWidgetsRetranslateUi;
        QMap<QAction*,QMap<QString,QString>> listActionsRetranslateUi;
        QMap<QPushButton*,QString> listDynamicPushButtonText;


        QToolBar *toolBar;
        QAction *accDictionary;
        QAction *accLanguage;
        QAction *accHelp;
        QAction *accInfo;
        QAction *accExit;

    protected:
        void resizeEvent(QResizeEvent */*e*/);
        void showEvent(QShowEvent *event);

    private slots:
        void clickButton();
        void clickButtonNext();

        void clickButtonDictionary();
        void clickButtonLanguage();
        void clickButtonInfo();
        void clickButtonHelp();
};

#endif // MAINWINDOW_H
