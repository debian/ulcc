#/bin/sh

FILES=`ls ../langs|grep properties|grep -v en.properties`

for STR in $FILES
do
  LANG=`echo $STR|sed 's|ulcc_||g'|sed 's|.properties||g'`
  rm -f ../dictionaries/*/languages/$LANG.lng
  cat ../langs/$STR | while read LINE
  do
     a=`echo $LINE|grep '#'`
     if [ ! -z "$a" ]
        then
           DIRNAME=`echo $LINE|grep '#'|sed 's|# ||g'`
           mkdir -p ../dictionaries/$DIRNAME/languages
        fi
     echo $LINE >> ../dictionaries/$DIRNAME/languages/$LANG.lng
  done
done
