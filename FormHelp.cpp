#include "FormHelp.h"
#include "ui_FormHelp.h"

#include "config_ulcc.h"

FormHelp::FormHelp(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FormHelp)
{
    ui->setupUi(this);
    ui->textEdit->setReadOnly(true);
    this->setWindowTitle(tr("Help"));
    this->setWindowIcon(QIcon(QString(GLOBAL_PATH_USERDATA)+"/images/icons/ulcc.png"));


    ui->textEdit->setText(tr("How to use:")+"\n\n"+
      tr("Choose the correct answer from the answers provided.\n")+
    "\n"
    );


    connect(ui->pushButton,SIGNAL(clicked()),this,SLOT(close()));

}

FormHelp::~FormHelp()
{
    delete ui;
}
